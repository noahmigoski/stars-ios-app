# Stars ios App

This is an ios app I made for elementary school specials teachers (music, art, PE) to keep track of their class good behavior rewards system.

## Use
To start using this app, download Pythonista from the app store and place this repository in its files. Then edit the file history.csv file to include the name of each class you teach seperated by a comma and run init.py. You can now make a shortcut for stars.py on the homescreen and use it like a normal app.