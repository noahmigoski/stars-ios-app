import ui
import sound

f = open('history.csv', 'r')
file = f.readlines()
f.close()
if len(file) > 2:
    pressed = int(len(file[-1]) / 3)
else:
    pressed = 0



def new_line():
    f = open('history.csv', 'r')
    length = len(f.readlines()[-1])
    f.close()
    if (length + 3) % 36 == 0:
        return True
    else:
        return False


def popcorn():
    label2.text = "🍿Popcorn!🍿"
    sound.play_effect('Ta Da-SoundBible.com-1884170640.mp3')


def column_total():
    f = open(column_name(), 'r')
    file = f.read()
    list = file.split(', ')
    f.close()
    del list[-1]
    ints = []
    for i in list:
        ints.append(int(i))
    return sum(ints)


def column_name():
    f = open('history.csv', 'r')
    file = f.readlines()
    f.close()
    global pressed
    return file[0].split(', ')[pressed]


def button_action(self):
    global pressed

    h = open(column_name(), 'a')
    h.write(self.title)
    h.write(', ')
    h.close

    if new_line():
        f = open('history.csv', 'a')
        f.write(self.title)
        f.write('  ')
        f.write('\n')
        f.close()
        update_label(int(self.title))
        pressed = 0
    else:
        f = open('history.csv', 'a')
        f.write(self.title)
        f.write(', ')
        f.close()
        update_label(int(self.title))
        pressed += 1


def current_label(self):
    global pressed
    label1.text = column_name() + " currently has " + str(
        column_total()) + " stars."
    label2.text = "    "


def update_label(addition):
    if column_total() + addition >= 25:
        label1.text = column_name() + " now has " + str(
            column_total() + addition - 25) + " stars."
        popcorn()
        g = open(column_name(), 'w')
        g.write("")
        g.close()
    else:
        label1.text = column_name() + " now has " + str(
            column_total() + addition) + " stars."


def no_school(self):
    global pressed
    if new_line():
        f = open('history.csv', 'a')
        f.write("0")
        f.write('  ')
        f.write('\n')
        f.close()
        update_label(int("0"))
        pressed = 0
    else:
        f = open('history.csv', 'a')
        f.write("0")
        f.write(', ')
        f.close()
        update_label(int("0"))
        pressed += 1
    label2.text = "    "


v = ui.load_view('stars')
v.present(orientations=['portrait'])
label1 = v['label1']
label2 = v['label2']